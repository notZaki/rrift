# Readme

This is a preprint of the manuscript:

> Pharmacokinetic modeling of dynamic contrast‐enhanced MRI using a reference region and input function tail  
> Zaki Ahmed & Ives R. Levesque  
> Magnetic Resonance in Medicine. [DOI: 10.1002/mrm.27913](https://doi.org/10.1002/mrm.27913)  

The general content here should be identical to the published version, but there could be editorial changes in the published version.

The preprint is available as a

- [html version](https://notzaki.gitlab.io/rrift/)
- [pdf version](https://notzaki.gitlab.io/rrift/_main.pdf)